package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

const (
	digits = "9876543210"
	need   = 200
)

type pragma struct {
	add, sub []int
}

func main() {
	p := &pragma{}

	result := p.getNumbers()

	fmt.Println("=== Pragmatic-200 ===")

	for _, res := range result {
		fmt.Println(res, "->", p.decodeOpersPrint(res), "=", need)
	}
}

func (p *pragma) decodeOpersPrint(res []int) string {
	var result strings.Builder

	result.WriteByte('9')

	for i, r := range res {
		switch r {
		case 0:
		case 1:
			result.WriteByte('+')
		case 2:
			result.WriteByte('-')
		default:
			continue
		}

		result.WriteByte(digits[i+1])
	}

	return result.String()
}

func (p *pragma) sumNumbers() int {
	var add, sub int

	for _, a := range p.add {
		add += a
	}

	for _, s := range p.sub {
		sub += s
	}

	return add - sub
}

func (p *pragma) getNumbers() [][]int {
	var result [][]int

	operations := make([][]int, 0, 19683)
	for i := 0; i < 19683; i++ { // 3^9 = 19683
		opers := make([]int, 9)
		operations = append(operations, fillOperations(i, opers))
	}

	for _, opers := range operations {
		index1, index2 := 0, 1
		lastOper := 1

		for i, op := range opers {
			var digit string

			switch op {
			case 0:
				index2++

				if i == len(opers)-1 {
					digit += digits[index1:index2]
					p.addDigit(digit, lastOper)
				}
			case 1, 2:
				digit = digits[index1:index2]
				p.addDigit(digit, lastOper)

				if op == 1 {
					lastOper = 1
				} else {
					lastOper = 2
				}

				index1 = index2
				index2++
			}
		}

		sum := p.sumNumbers()
		fmt.Println(opers, "->", p.add, p.sub, "->", p.decodeOpersPrint(opers), "=", sum)

		p.add, p.sub = []int{}, []int{}

		if sum == need {
			result = append(result, opers)
		}
	}

	return result
}

func (p *pragma) addDigit(digit string, oper int) {
	dig, err := strconv.Atoi(digit)
	if err != nil {
		log.Fatal(err) // bad but simple
	}

	if oper == 1 {
		p.add = append(p.add, dig)
	} else if oper == 2 {
		p.sub = append(p.sub, dig)
	}
}

func fillOperations(n int, in []int) []int {
	for i := 8; n > 0; i-- {
		in[i] = n % 3
		n /= 3
	}

	return in
}
